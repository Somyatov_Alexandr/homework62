import React from 'react';

const ContactPage = props => {
  return (
      <div className="contactPage">
        <h1 className="main__heading">Contact</h1>
        <div className="contact__list">
          <div className="contact__phone">+996(555) 55 55 55</div>
          <div className="contact__address">Bishkek Chyi st. 225</div>
        </div>
      </div>
  );
};

export default ContactPage;