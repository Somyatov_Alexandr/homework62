import React, {Component} from 'react';
import {Route, NavLink, Switch} from "react-router-dom";
import HomePage from "../../components/HomePage/HomePage";
import AboutPage from "../../components/AboutPage/AboutPage";
import ContactPage from "../../components/ContactPage/ContactPage";
import './PageBuilder.css';



class PageBuilder extends Component {


  render () {
    return (
        <div className="wrapper">
          <menu className="top-nav">
            <li className="top-nav__item">
              <NavLink
                  activeClassName="top-nav__active"
                  exact className="top-nav__link"
                  to="/">Home</NavLink>
            </li>
            <li className="top-nav__item">
              <NavLink
                  activeClassName="top-nav__active"
                  className="top-nav__link"
                  to="/about">About</NavLink>
            </li>
            <li className="top-nav__item">
              <NavLink
                  activeClassName="top-nav__active"
                  className="top-nav__link"
                  to="/contact">Contact</NavLink>
            </li>
          </menu>
          <Switch>
            <Route path="/" exact component={HomePage}/>
            <Route path="/about" exact component={AboutPage}/>
            <Route path="/contact" exact component={ContactPage}/>
          </Switch>

        </div>
    );
  }
};

export default PageBuilder;