import React from 'react';
import './AboutPage.css';

const AboutPage = props => {
  return (
      <div className="aboutPage">
        <h1 className="about__heading main__heading">About us</h1>
        <div className="about-img__wrap">
          <img className="img-responsive" src="img/about.jpg" alt="about us"/>
        </div>
        <div className="about__text">
          <p>As we worked on React 16, we revamped the folder structure and much of the build tooling in the React repository. Among other things, we introduced projects such as Rollup, Prettier, and Google Closure Compiler into our workflow. People often ask us questions about how we use those tools. In this post, we would like to share some of the changes that we’ve made to our build and test infrastructure in 2017, and what motivated them.</p>
        </div>
      </div>
  );
};

export default AboutPage;