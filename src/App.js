import React, { Component } from 'react';
import PageBuilder from "./containers/PageBuilder/PageBuilder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <PageBuilder/>
      </div>
    );
  }
}

export default App;
