import React, {Component} from 'react';
import PostItem from "../PostItem/PostItem";
import './HomePage.css';

class HomePage extends Component {
  state = {
    posts: []
  };

  componentDidMount = () => {
    const POST_URL = 'https://jsonplaceholder.typicode.com/posts?_limit=6';

    fetch(POST_URL).then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('Something went wrong with network request');
    }).then(posts => {
      const updatedPosts = posts.map(post => {
        return {...post};
      });

      this.setState({posts: updatedPosts});

    }).catch(error => {
      console.log(error);
    })
  };

  render(){
    return (
        <div className="homePage">
          <h1 className="main__heading">Popular posts</h1>
          <div className="posts__list">
            {this.state.posts.map(post => (
                <PostItem
                    key={post.id}
                    title={post.title}
                    body={post.body}
                />
            ))}
          </div>
        </div>
    );
  }

};

export default HomePage;