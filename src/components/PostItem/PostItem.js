import React from 'react';
import './PostItem.css';

const PostItem = props => {
  return (
      <div className="post__item">
        <h2 className="post__heading">{props.title}</h2>
        <div className="post__body">{props.body}</div>
      </div>
  );
};

export default PostItem;